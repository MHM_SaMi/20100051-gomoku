package gomoku.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;


public class FindCompetitor extends Composite {
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	VerticalPanel vPanel = new VerticalPanel();
	HorizontalPanel hPanel;
	public FindCompetitor() {		
		this.initWidget(this.vPanel);
		vPanel.setBorderWidth(1);
		hPanel = new HorizontalPanel();
		Label label = new Label("enter competitor name");
		final TextBox text = new TextBox();
		hPanel.add(label);		hPanel.add(text);		vPanel.add(hPanel);
		Button sendAnInvitation = new Button("send an invitaion!");
		sendAnInvitation.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				final String senderName = playersInfo.get_player1_name();
				String recieverName = text.getText();
				greetingService.sendAnInvitation(senderName, recieverName, new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						RootPanel.get().add(new HTML("RPC call failed. :-("));
					}
					public void onSuccess(String result) {
						if(result.equals("success")) {	
							final Timer myTimer = new Timer() {
								public void schedule(int delayMillis) {
									super.schedule(delayMillis);
								}	
								public void scheduleRepeating(int periodMillis) {
									super.scheduleRepeating(periodMillis);
								}
								public void cancel() {
									super.cancel();
								}
								public void run() {
									greetingService.checkAcceptedRequsts(senderName, new AsyncCallback<String>() {
										public void onSuccess(String result) {
											if(!result.equals("0")) {
												greetingService.cancelAnInvitation(senderName, result, new AsyncCallback<String>() {
													public void onSuccess(String result) {
														
													}
													public void onFailure(Throwable caught) {
														RootPanel.get().add(new HTML("RPC call failed. :-("));
													}
												});
												playersInfo.set_player2_name(result);
												if(playersInfo.get_player1_color().equals("White")) {
													playersInfo.set_player2_color("Black");
												}
												else if(playersInfo.get_player1_color().equals("Black")) {
													playersInfo.set_player2_color("White");
												}
												openGameBoardOnline();
												cancel();
											}
										}
										public void onFailure(Throwable caught) {
											RootPanel.get().add(new HTML("RPC call failed. :-("));
										}
									});
								}
							};
							myTimer.schedule(10000);
							myTimer.scheduleRepeating(10000);
							myTimer.run();
						}
						else {
							RootPanel.get().add(new HTML(result));
						}
					}
				});
			}
		});
		vPanel.add(sendAnInvitation);
	}
	
	public void openGameBoardOnline() {
		RootPanel.get().clear();
		GameBoardOnline gameBoardOnline = new GameBoardOnline();
		RootPanel.get().add(gameBoardOnline);
	}
}