package gomoku.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GameBoard extends Composite {
	private final GreetingServiceAsync greetingService
	= GWT.create(GreetingService.class);
	private Grid grid;
	private VerticalPanel vPanel = new VerticalPanel();
	private String[][] gameMatrix;
	private boolean current;
	private int gameCotroller;
	private String winnerMessage;
	private boolean isGameEnd;
	
	private String getCurrentPlayerChar() {
		if (current == true) {
			return playersInfo.getPlayer1Char();
		} else return playersInfo.getPlayer2Char();
	}
	
	String getCurrentPlayerName() {
		if (getCurrentPlayerChar() == "X") {
			return playersInfo.get_player1_name();
		}
		else return playersInfo.get_player2_name();
	}

	private boolean _count5(int r, int dr, int c, int dc) {
		for (int i = 1; i < 5; i++) {
			int row = r + dr * i, col = c + dc * i;
			if (row >= 16 || row < 0 || col >= 16 || col < 0) return false;
			else if (!gameMatrix[r + dr * i][c + dc * i].equals(getCurrentPlayerChar())) return false;
		}
		return true; // There were 5 in a row!
	}

	public GameBoard() {
		this.initWidget(this.vPanel);
		grid = new Grid(16, 16);
		final int numRows = grid.getRowCount();
		final int numColumns = grid.getColumnCount();
		gameMatrix = new String[16][16];
		for (int r = 0; r < gameMatrix.length; r++) {
			for (int c = 0; c < gameMatrix[0].length; c++) {
				gameMatrix[r][c] = "";
			}
		}
		current = true;
		gameCotroller = 0;
		isGameEnd = false;
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numColumns; j++) {
				String text = "", row = "", col = "";
				for (int r = 0; r < i; r++, row += " ");
				for (int c = 0; c < j; c++, col += " ");
				text += row + "\n" + col;
				final Button button = new Button(text);
				button.setSize("25px", "25px");
				grid.setWidget(i, j, button);
				button.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						if (isGameEnd == false) {
							if (!button.getText().equals("X") && !button.getText().equals("O")) {
								String text = button.getText();
								int row = 0, col = 0;
								for (int i = 0; i < text.length(); i++, row++) {
									if (text.charAt(i) == '\n') {
										for (int j = i + 1; j < text.length(); j++, col++);
										break;
									}
								}
								
								gameMatrix[row][col] = getCurrentPlayerChar();
								
								if (_count5(row, 0, col, 1) || _count5(row, 0, col, -1)
									|| _count5(row, 1, col, 0) || _count5(row, -1, col, 0)
									|| _count5(row, 1, col, 1) || _count5(row, -1, col, -1)
									|| _count5(row, 1, col, -1) || _count5(row, -1, col, 1)) {
									isGameEnd = true;
									winnerMessage = "Player "+getCurrentPlayerName()+" WINS!";
									RootPanel.get().add(new HTML(winnerMessage));
								}
								
								button.setText(getCurrentPlayerChar());
								current = !current;
								gameCotroller ++;
							}
							
							if (gameCotroller == (numRows * numColumns)) {
								isGameEnd = true;
								winnerMessage = "Game is a Tie! No Winner";
								RootPanel.get().add(new HTML(winnerMessage));
							}
						}
						else {
							RootPanel.get().add(new HTML(winnerMessage));
						}
					}
				});
			}
		}
		Button playAgainBtn = new Button("Play Again");
		playAgainBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				openGameBoard();
			}
		});
		Button closeBtn = new Button("Close");
		closeBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				RootPanel.get().add(new HTML("See you later :-)"));
				greetingService.systemClose(new AsyncCallback<Void>() {
					public void onSuccess(Void result) {
						
					}
					public void onFailure(Throwable caught) {
						RootPanel.get().add(new HTML("RPC call failed. :-("));
					}
				});
			}
		});
		vPanel.add(grid);
		vPanel.add(playAgainBtn);
		vPanel.add(closeBtn);
	}
	
	public void openGameBoard() {
		RootPanel.get().clear();
		GameBoard gameBoard = new GameBoard();
		RootPanel.get().add(gameBoard);
	}
}