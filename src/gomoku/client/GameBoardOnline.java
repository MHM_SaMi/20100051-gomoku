package gomoku.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GameBoardOnline extends Composite {
	private final GreetingServiceAsync greetingService
	= GWT.create(GreetingService.class);
	private Grid grid;
	private VerticalPanel vPanel = new VerticalPanel();
	private String[][] gameMatrix;
	private final String myName;
	private final String playerOneName;
	private final String playerTwoName;
	private final ArrayList<String> lastTurnPlace;
	private boolean current;
	private int gameCotroller1;
	private int gameCotroller2;
	private int gameCotroller3;
	private String winnerMessage;
	private boolean isGameEnd;
	
	private String getCurrentPlayerChar() {
		if (current == true) {
			return playersInfo.getPlayer1Char();
		} else return playersInfo.getPlayer2Char();
	}
	
	String getCurrentPlayerName() {
		if (getCurrentPlayerChar() == "X") {
			return playersInfo.get_player1_name();
		}
		else return playersInfo.get_player2_name();
	}
	
	private boolean _count5(int r, int dr, int c, int dc) {
		for (int i = 1; i < 5; i++) {
			int row = r + dr * i, col = c + dc * i;
			if (row >= 16 || row < 0 || col >= 16 || col < 0) return false;
			else if (!gameMatrix[r + dr * i][c + dc * i].equals(getCurrentPlayerChar())) return false;
		}
		return true; // There were 5 in a row!
	}

	public GameBoardOnline() {
		this.initWidget(this.vPanel);
		grid = new Grid(16, 16);
		final int numRows = grid.getRowCount();
		final int numColumns = grid.getColumnCount();
		gameMatrix = new String[16][16];
		for (int r = 0; r < gameMatrix.length; r++) {
			for (int c = 0; c < gameMatrix[0].length; c++) {
				gameMatrix[r][c] = "";
			}
		}
		myName = playersInfo.get_my_name();
		playerOneName = playersInfo.get_player1_name();
		playerTwoName = playersInfo.get_player2_name();
		lastTurnPlace = new ArrayList<String>();
		lastTurnPlace.add("empty");
		current = true;
		gameCotroller1 = 0;
		gameCotroller2 = 0;
		gameCotroller3 = 0;
		isGameEnd = false;
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numColumns; j++) {
				String text = "", row = "", col = "";
				for (int r = 0; r < i; r++, row += " ");
				for (int c = 0; c < j; c++, col += " ");
				text += row + "\n" + col;
				final Button button = new Button(text);
				button.setSize("25px", "25px");
				grid.setWidget(i, j, button);
				button.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						if (isGameEnd == false) {
							if (getCurrentPlayerName().equals(myName) || ((gameCotroller1 == gameCotroller2) && (myName.equals(playerOneName)))
										|| ((gameCotroller2 > gameCotroller1) && (myName.equals(playerTwoName)))) {
									if (!button.getText().equals("X") && !button.getText().equals("O")) {
									String text = button.getText();
									button.setText(getCurrentPlayerChar());
									int row = 0, col = 0;
									for (int i = 0; i < text.length(); i++, row++) {
										if (text.charAt(i) == '\n') {
											for (int j = i + 1; j < text.length(); j++, col++);
											break;
										}
									}
									
									gameMatrix[row][col] = getCurrentPlayerChar();
									
									if (_count5(row, 0, col, 1) || _count5(row, 0, col, -1)
										|| _count5(row, 1, col, 0) || _count5(row, -1, col, 0)
										|| _count5(row, 1, col, 1) || _count5(row, -1, col, -1)
										|| _count5(row, 1, col, -1) || _count5(row, -1, col, 1)) {
										isGameEnd = true;
										winnerMessage = "Player "+getCurrentPlayerName()+" WINS!";
										RootPanel.get().add(new HTML(winnerMessage));
									}
									
									if (getCurrentPlayerName().equals(myName)) {
										gameCotroller1++;
										lastTurnPlace.add(row + "-" + col);
										greetingService.changeTurnPlace(playerOneName, playerTwoName, row + "-" + col, new AsyncCallback<String>() {
											public void onSuccess(String result) {
												
											}
											public void onFailure(Throwable caught) {
												RootPanel.get().add(new HTML("RPC call failed. :-("));
											}
										});
									}
									current = !current;
									gameCotroller3 ++;
								}
							}
							
							else {
								RootPanel.get().add(new HTML("This is NOT your turn!!! PLEASE WAIT..."));
							}
							
							if (gameCotroller3 == (numRows * numColumns)) {
								isGameEnd = true;
								winnerMessage = "Game is a Tie! No Winner";
								RootPanel.get().add(new HTML(winnerMessage));
							}
						}
						else {
							RootPanel.get().add(new HTML(winnerMessage));
						}
					}
				});
			}
		}
		Timer myTimer = new Timer() {
			public void schedule(int delayMillis) {
				super.schedule(delayMillis);
			}	
			public void scheduleRepeating(int periodMillis) {
				super.scheduleRepeating(periodMillis);
			}
			public void run() {
				if (!getCurrentPlayerName().equals(myName)) {
					greetingService.getTurnPlace(playerOneName, playerTwoName, new AsyncCallback<String>() {
						public void onSuccess(String result) {
							if (!result.equals(lastTurnPlace.get(lastTurnPlace.size()-1))) {
								lastTurnPlace.add(result);
								String rowString = "", colString = "";
								for (int i = 0; i < result.length(); i++) {
									if (result.charAt(i) == '-') {
										for (int j = i+1; j < result.length(); j++) {
											colString += result.charAt(j);
										}
										break;
									}
									else rowString += result.charAt(i);
								}
								int rowInt = Integer.valueOf(rowString);
								int colInt = Integer.valueOf(colString);
								gameCotroller2 ++;
								Button b = (Button)grid.getWidget(rowInt, colInt);
								b.click();
							}
						}
						public void onFailure(Throwable caught) {
							RootPanel.get().add(new HTML("RPC call failed. :-("));
						}
					});
				}
			}
		};
		myTimer.schedule(5000);
		myTimer.scheduleRepeating(5000);
		myTimer.run();
		Button playAgainBtn = new Button("Play Again");
		playAgainBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				String myColor = playersInfo.get_my_color();
				playersInfo.set_player1_name(myName);
				playersInfo.set_player1_color(myColor);
				greetingService.playAgain(myName, new AsyncCallback<String>() {
					public void onSuccess(String result) {
						openFindCompetitor();
					}
					public void onFailure(Throwable caught) {
						RootPanel.get().add(new HTML("RPC call failed. :-("));
					}
				});
			}
		});
		Button closeBtn = new Button("Close");
		closeBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				RootPanel.get().add(new HTML("See you later :-)"));
				greetingService.logOutFromServer(myName, new AsyncCallback<String>() {
					public void onSuccess(String result) {
						
					}
					public void onFailure(Throwable caught) {
						RootPanel.get().add(new HTML("RPC call failed. :-("));
					}
				});
			}
		});
		vPanel.add(grid);
		vPanel.add(playAgainBtn);
		vPanel.add(closeBtn);
	}
	
	public void openFindCompetitor() {
		RootPanel.get().clear();
		FindCompetitor findCompetitor = new FindCompetitor();
		RootPanel.get().add(findCompetitor);
	}
}