package gomoku.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GameMode extends Composite {
	private VerticalPanel vPanel = new VerticalPanel();

	public GameMode() {
		this.initWidget(this.vPanel);
		vPanel.setBorderWidth(1);
		Button localMode = new Button("Local Mode");
		localMode.addClickHandler(new localModeClickHandler());
		this.vPanel.add(localMode);
		Button onlineMode = new Button("Online Mode");
		onlineMode.addClickHandler(new onlineModeClickHandler());
		this.vPanel.add(onlineMode);
	}

	private class localModeClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			openLoginLocal();
		}
	}

	private class onlineModeClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			openLoginOnline();
		}
	}

	public void openLoginLocal() {
		this.vPanel.clear();
		LocalLogin localLogin = new LocalLogin();
		this.vPanel.add(localLogin);
	}

	public void openLoginOnline() {
		this.vPanel.clear();
		OnlineLogin onlineLogin = new OnlineLogin();
		this.vPanel.add(onlineLogin);
	}
}