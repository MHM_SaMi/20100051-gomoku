package gomoku.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GameStart extends Composite {
	private VerticalPanel vPanel = new VerticalPanel();

	public GameStart() {
		this.initWidget(this.vPanel);
		vPanel.setBorderWidth(1);
		Button startGameBtn = new Button("Start Game");
		startGameBtn.addClickHandler(new startGameBtnClickHandler());
		vPanel.add(startGameBtn);
	}

	private class startGameBtnClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			openPlayMode();
		}
	}

	public void openPlayMode() {
		this.vPanel.clear();
		GameMode gameMode = new GameMode();
		this.vPanel.add(gameMode);
	}
}