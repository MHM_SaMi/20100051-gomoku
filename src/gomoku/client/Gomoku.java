package gomoku.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

public class Gomoku implements EntryPoint {
	public void onModuleLoad() {
		MainView main = new MainView();
		RootPanel.get().add(main);
	}
}