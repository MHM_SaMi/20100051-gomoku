package gomoku.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * 
 * The client side stub for the RPC service.
 */

@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
	String logIntoServer(String name) throws IllegalArgumentException;
	String changePlayerColor(String name, String color) throws IllegalArgumentException;
	String sendAnInvitation(String senderName, String competitorName) throws IllegalArgumentException;
	String cancelAnInvitation(String senderName, String recieverName) throws IllegalArgumentException;
	String acceptAnInvitation(String senderName, String recieverName) throws IllegalArgumentException;
	String checkWaitedRequsts(String playerName) throws IllegalArgumentException;
	String checkAcceptedRequsts(String playerName) throws IllegalArgumentException;
	String changePlayerStatusToBusy(String playerName) throws IllegalArgumentException;
	String changePlayerStatusToAvailable(String playerName) throws IllegalArgumentException;
	String checkPlayerAvailability(String playerName) throws IllegalArgumentException;
	String checkPlayerColor(String playerName) throws IllegalArgumentException;
	String getTurnPlace(String playerOneName, String playerTwoName) throws IllegalArgumentException;
	String changeTurnPlace(String playerOneName, String playerTwoName, String newTurnPlace) throws IllegalArgumentException;
	String playAgain(String playerName) throws IllegalArgumentException;
	String logOutFromServer(String playerName) throws IllegalArgumentException;
	void systemClose() throws IllegalArgumentException;
}