package gomoku.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * 
 * The async counterpart of GreetingService.
 */

@SuppressWarnings("rawtypes")
public interface GreetingServiceAsync {
	public void logIntoServer(String name, AsyncCallback callback) throws IllegalArgumentException;
	public void changePlayerColor(String name, String color, AsyncCallback callback) throws IllegalArgumentException;
	public void sendAnInvitation(String senderName, String competitorName, AsyncCallback<String> asyncCallback) throws IllegalArgumentException;
	public void cancelAnInvitation(String senderName, String recieverName, AsyncCallback<String> callback) throws IllegalArgumentException;
	public void acceptAnInvitation(String senderName, String recieverName, AsyncCallback<String> callback) throws IllegalArgumentException;
	public void checkWaitedRequsts(String playerName, AsyncCallback asyncCallback) throws IllegalArgumentException;
	public void checkAcceptedRequsts(String playerName, AsyncCallback<String> callback) throws IllegalArgumentException;
	public void changePlayerStatusToBusy(String playerName, AsyncCallback<String> callback) throws IllegalArgumentException;
	public void changePlayerStatusToAvailable(String playerName, AsyncCallback<String> callback) throws IllegalArgumentException;
	public void checkPlayerAvailability(String playerName, AsyncCallback<String> callback) throws IllegalArgumentException;
	public void checkPlayerColor(String playerName, AsyncCallback<String> callback) throws IllegalArgumentException;
	public void getTurnPlace(String playerOneName, String playerTwoName, AsyncCallback<String> callback) throws IllegalArgumentException;
	public void changeTurnPlace(String playerOneName, String playerTwoName, String newTurnPlace, AsyncCallback<String> callback) throws IllegalArgumentException;
	public void playAgain(String playerName, AsyncCallback<String> callback) throws IllegalArgumentException;
	public void logOutFromServer(String playerName, AsyncCallback<String> callback) throws IllegalArgumentException;
	public void systemClose(AsyncCallback<Void> callback) throws IllegalArgumentException;
}