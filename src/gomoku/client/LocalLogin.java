package gomoku.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class LocalLogin extends Composite {
	VerticalPanel vPanel = new VerticalPanel();
	HorizontalPanel hPanel1, hPanel2;

	public LocalLogin() {
		this.initWidget(this.vPanel);
		vPanel.setBorderWidth(1);
		hPanel1 = new HorizontalPanel();
		hPanel2 = new HorizontalPanel();
		Label label1 = new Label("enter player 1 username");
		Label label2 = new Label("enter player 2 username");
		final TextBox text1 = new TextBox();
		final TextBox text2 = new TextBox();
		hPanel1.add(label1);
		hPanel1.add(text1);
		vPanel.add(hPanel1);
		hPanel2.add(label2);
		hPanel2.add(text2);
		vPanel.add(hPanel2);
		Button playerColorBtn = new Button("Choose Player Color");
		playerColorBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				String name1 = text1.getText();
				playersInfo.set_player1_name(name1);
				String name2 = text2.getText();
				playersInfo.set_player2_name(name2);
				openPlayerColor();
			}
		});
		vPanel.add(playerColorBtn);
	}

	public void openPlayerColor() {
		this.vPanel.clear();
		PlayerColor playerColor = new PlayerColor();
		this.vPanel.add(playerColor);
	}
}