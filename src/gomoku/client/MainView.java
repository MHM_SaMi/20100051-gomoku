package gomoku.client;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;

public class MainView extends Composite {
	private VerticalPanel vPanel = new VerticalPanel();

	public MainView() {
		initWidget(this.vPanel);
		openStartGame();
	}

	public void openStartGame() {
		this.vPanel.clear();
		GameStart gameStart = new GameStart();
		this.vPanel.add(gameStart);
	}
}
