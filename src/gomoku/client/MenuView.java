package gomoku.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;

public class MenuView extends Composite {
	private HorizontalPanel hPanel = new HorizontalPanel();

	public MenuView() {
		initWidget(this.hPanel);
		Button startGameBtn = new Button("Start Game");
		startGameBtn.addClickHandler(new startGameBtnClickHandler());
		hPanel.add(startGameBtn);
	}

	private class startGameBtnClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			
		}
	}
}
