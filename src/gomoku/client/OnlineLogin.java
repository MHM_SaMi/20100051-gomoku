package gomoku.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

class MyPopup extends PopupPanel {
	private final GreetingServiceAsync greetingService 
		= GWT.create(GreetingService.class);
	public MyPopup(final String senderName) {
		super(true);
		final String recieverName = playersInfo.get_player1_name();
		VerticalPanel vPanel = new VerticalPanel();
		HorizontalPanel hPanel = new HorizontalPanel();
		Button acceptBtn = new Button("Accept");
		acceptBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				greetingService.acceptAnInvitation(senderName, recieverName, new AsyncCallback<String>() {
					public void onSuccess(String result) {
						if(result.equals("success")) {
							playersInfo.set_player1_name(senderName);
							playersInfo.set_player2_name(recieverName);
							greetingService.checkPlayerColor(senderName, new AsyncCallback<String>() {
								public void onSuccess(String result) {
									if(result.equals("White") || result.equals("Black")) {
										playersInfo.set_player1_color(result);
										if(result.equals("White")) {
											playersInfo.set_player2_color("Black");
										}
										else if(result.equals("Black")) {
											playersInfo.set_player2_color("White");
										}
										openGameBoardOnline();
									}
								}
								public void onFailure(Throwable caught) {
									RootPanel.get().add(new HTML("RPC call failed. :-("));
								}
							});
						}
						RootPanel.get().add(new HTML(result));
					}
					public void onFailure(Throwable caught) {
						RootPanel.get().add(new HTML("RPC call failed. :-("));
					}
				});
			}
		});
		Button rejectBtn = new Button("Reject");
		rejectBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				greetingService.cancelAnInvitation(senderName, recieverName, new AsyncCallback<String>() {
					public void onSuccess(String result) {
						RootPanel.get().add(new HTML(result));
					}
					public void onFailure(Throwable caught) {
						RootPanel.get().add(new HTML("RPC call failed. :-("));
					}
				});
			}
		});
		vPanel.add(new Label("You recieve an invitation from "+ senderName));
		hPanel.add(acceptBtn);
		hPanel.add(rejectBtn);
		vPanel.add(hPanel);
		setWidget(vPanel);
	}
	public void openGameBoardOnline() {
		RootPanel.get().clear();
		GameBoardOnline gameBoardOnline = new GameBoardOnline();
		RootPanel.get().add(gameBoardOnline);
	}
}

public class OnlineLogin extends Composite {
	private final GreetingServiceAsync greetingService
		= GWT.create(GreetingService.class);
	VerticalPanel vPanel = new VerticalPanel();
	HorizontalPanel hPanel;
	public OnlineLogin() {
		this.initWidget(this.vPanel);
		vPanel.setBorderWidth(1);
		hPanel = new HorizontalPanel();
		Label label = new Label("enter your username");
		final TextBox text = new TextBox();
		hPanel.add(label);
		hPanel.add(text);
		vPanel.add(hPanel);
		Button playerColorOnlineBtn = new Button("Choose Player Color");
		playerColorOnlineBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				final String name = text.getText();
				greetingService.logIntoServer(name, new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						RootPanel.get().add(new HTML("RPC call failed. :-("));
					}
					public void onSuccess(String result) {
						if(result.equals("success")) {
							playersInfo.set_player1_name(name);
							playersInfo.set_my_name(name);
							Timer myTimer = new Timer() {
								String playerName = playersInfo.get_player1_name();
								public void schedule(int delayMillis) {
									super.schedule(delayMillis);
								}	
								public void scheduleRepeating(int periodMillis) {
									super.scheduleRepeating(periodMillis);
								}
								public void run() {
									greetingService.checkWaitedRequsts(playerName, new AsyncCallback<String>() {
										public void onFailure(Throwable caught) {
											RootPanel.get().add(new HTML("RPC call failed. :-("));
										}
										public void onSuccess(String result) {
											if (!result.equals("0")) {
												MyPopup myPopup = new MyPopup(result);
												RootPanel.get().add(myPopup);
											}
										}
									});
								}
							};
							myTimer.schedule(10000);
							myTimer.scheduleRepeating(10000);
							myTimer.run();
							openPlayerColorOnline();
						}
						RootPanel.get().add(new HTML(result));
					}
				});
			}
		});
		vPanel.add(playerColorOnlineBtn);
	}

	public void openPlayerColorOnline() {
		this.vPanel.clear();
		PlayerColorOnline playerColorOnline = new PlayerColorOnline();
		this.vPanel.add(playerColorOnline);
	}
}