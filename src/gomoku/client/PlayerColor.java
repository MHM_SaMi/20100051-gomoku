package gomoku.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;

public class PlayerColor extends Composite {
	VerticalPanel vPanel = new VerticalPanel();
	HorizontalPanel hPanel1, hPanel2;

	public PlayerColor() {
		this.initWidget(this.vPanel);
		vPanel.setBorderWidth(1);

		hPanel1 = new HorizontalPanel();
		Label label1 = new Label("Player 1 color");
		final RadioButton black1 = new RadioButton("player1", "Black");
		final RadioButton white1 = new RadioButton("player1", "White");
		hPanel1.add(label1);
		hPanel1.add(black1);
		hPanel1.add(white1);
		vPanel.add(hPanel1);

		hPanel2 = new HorizontalPanel();
		Label label2 = new Label("Player 2 color");
		final RadioButton black2 = new RadioButton("player2", "Black");
		final RadioButton white2 = new RadioButton("player2", "White");
		hPanel2.add(label2);
		hPanel2.add(black2);
		hPanel2.add(white2);
		vPanel.add(hPanel2);

		black1.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				white1.setEnabled(false);
				black2.setEnabled(false);
				black1.setValue(true);
				white2.setValue(true);
			}
		});

		white1.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				black1.setEnabled(false);
				white2.setEnabled(false);
				white1.setValue(true);
				black2.setValue(true);
			}
		});

		black2.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				white2.setEnabled(false);
				black1.setEnabled(false);
				black2.setValue(true);
				white1.setValue(true);
			}
		});

		white2.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				black2.setEnabled(false);
				white1.setEnabled(false);
				white2.setValue(true);
				black1.setValue(true);
			}
		});
		Button gameBoardBtn = new Button("Let's Play!");
		gameBoardBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (white1.getValue() == true) {
					playersInfo.set_player1_color("White");
					playersInfo.set_player2_color("Black");
				} else if (black1.getValue() == true) {
					playersInfo.set_player1_color("Black");
					playersInfo.set_player2_color("White");
				}
				openGameBoard();
			}
		});
		vPanel.add(gameBoardBtn);
	}

	public void openGameBoard() {
		this.vPanel.clear();
		GameBoard gameBoard = new GameBoard();
		this.vPanel.add(gameBoard);
	}
}