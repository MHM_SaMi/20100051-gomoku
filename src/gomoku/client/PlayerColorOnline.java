package gomoku.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class PlayerColorOnline extends Composite {
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	VerticalPanel vPanel = new VerticalPanel();
	HorizontalPanel hPanel;
	public PlayerColorOnline() {
		this.initWidget(this.vPanel);
		vPanel.setBorderWidth(1);
		hPanel = new HorizontalPanel();
		Label label = new Label("enter your color");
		final RadioButton black = new RadioButton("player", "Black");
		final RadioButton white = new RadioButton("player", "White");
		hPanel.add(label);
		hPanel.add(black);
		hPanel.add(white);
		vPanel.add(hPanel);

		black.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				white.setEnabled(false);
			}
		});

		white.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				black.setEnabled(false);
			}
		});

		Button findCompetitorBtn = new Button("Let's Play!");
		findCompetitorBtn.addClickHandler(new ClickHandler() {
			@SuppressWarnings("rawtypes")
			public void onClick(ClickEvent event) {
				if (white.getValue() == true) {
					playersInfo.set_player1_color("White");
					playersInfo.set_my_color("White");
					String playerName = playersInfo.get_player1_name();
					String playerColor = playersInfo.get_player1_color();
					greetingService.changePlayerColor(playerName, playerColor, new AsyncCallback() {
								public void onFailure(Throwable caught) {
									RootPanel.get().add(new HTML("RPC call failed. :-("));
								}
								public void onSuccess(Object result) {
									RootPanel.get().add(new HTML("RPC call Success:"));
									RootPanel.get().add(new HTML(playersInfo.get_player1_name()));
									RootPanel.get().add(new HTML(playersInfo.get_player1_color()));
								}
							});
				} else if (black.getValue() == true) {
					playersInfo.set_player1_color("Black");
					playersInfo.set_my_color("Black");
					String playerName = playersInfo.get_player1_name();
					String playerColor = playersInfo.get_player1_color();
					greetingService.changePlayerColor(playerName, playerColor, new AsyncCallback() {
								public void onFailure(Throwable caught) {
									RootPanel.get().add(new HTML("RPC call failed. :-("));
								}
								public void onSuccess(Object result) {
									RootPanel.get().add(new HTML("RPC call Success:"));
									RootPanel.get().add(new HTML(playersInfo.get_player1_name()));
									RootPanel.get().add(new HTML(playersInfo.get_player1_color()));
								}
							});
				}
				openFindCompetitor();
			}
		});
		vPanel.add(findCompetitorBtn);
	}

	public void openFindCompetitor() {
		this.vPanel.clear();
		FindCompetitor findCompetitor = new FindCompetitor();
		this.vPanel.add(findCompetitor);
	}
}