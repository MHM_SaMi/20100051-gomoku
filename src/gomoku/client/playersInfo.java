package gomoku.client;

public class playersInfo {
	private static String _my_name;
	private static String _my_color;
	private static String _player1_name;
	private static String _player1_id;
	private static String _player1_color;
	private final static String _player1_char = "X";
	private static String _player2_name;
	private static String _player2_id;
	private static String _player2_color;
	private final static String _player2_char = "O";

	public static String get_player1_name() {
		return _player1_name;
	}

	public static void set_player1_name(String player1_name) {
		_player1_name = player1_name;
	}

	public static String get_player1_id() {
		return _player1_id;
	}

	public static void set_player1_id(String player1_id) {
		_player1_id = player1_id;
	}

	public static String get_player1_color() {
		return _player1_color;
	}

	public static void set_player1_color(String player1_color) {
		_player1_color = player1_color;
	}

	public static String get_player2_name() {
		return _player2_name;
	}

	public static void set_player2_name(String player2_name) {
		_player2_name = player2_name;
	}
	
	public static String getPlayer1Char() {
		return _player1_char;
	}

	public static String get_player2_id() {
		return _player2_id;
	}

	public static void set_player2_id(String player2_id) {
		_player2_id = player2_id;
	}

	public static String get_player2_color() {
		return _player2_color;
	}

	public static void set_player2_color(String player2_color) {
		_player2_color = player2_color;
	}

	public static String getPlayer2Char() {
		return _player2_char;
	}

	public static String get_my_name() {
		return _my_name;
	}

	public static void set_my_name(String my_name) {
		_my_name = my_name;
	}

	public static String get_my_color() {
		return _my_color;
	}

	public static void set_my_color(String my_color) {
		_my_color = my_color;
	}
}