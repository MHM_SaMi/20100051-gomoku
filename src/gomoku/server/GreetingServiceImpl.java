package gomoku.server;

import gomoku.client.GreetingService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {
	String dbUrl = "jdbc:mysql://144.76.19.105:3306/dbgomoku";
	String USER = "mohammed";
    String PASS = "1020304050";
	String dbClass = "com.mysql.jdbc.Driver";
	
	public String logIntoServer(String name) throws IllegalArgumentException {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if (con == null) {
				return "Fail to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			String query = "select name from player where name = '" + name + "'";
			ResultSet rs = stmt.executeQuery(query);
			rs.last();
			int rows = rs.getRow();
			if (rows != 0) {
				con.close();
				return "Player "+ name + " is already exist!\n"
						+ "Please, choose another name.";
			}
			String query2 = "insert into player (name) values (" + "'" + name + "')";
			stmt.executeUpdate(query2);
			con.close();
			return "success";
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}
	
	public String changePlayerColor(String name, String color) throws IllegalArgumentException {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if (con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			String query = "update player set color = '" + color 
					+ "' where name = '" + name + "'";
			stmt.executeUpdate(query);
			con.close();
			return "success";
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}
	
	public String sendAnInvitation(String senderName, String competitorName) throws IllegalArgumentException {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if (con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			if(!checkPlayerAvailability(competitorName).equals("success")) {
				return "Player "+ competitorName + " is not available!";
			}
			String query = "insert into invitations (senderName, recieverName, status) "
					+ "values ('" + senderName + "', '" + competitorName + "', 'waited')";
			stmt.executeUpdate(query);
			con.close();
			return "success";
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}
	
	public String cancelAnInvitation(String senderName, String recieverName) {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if(con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			String query = "update invitations set status = 'canceled' where senderName = '"
					+ senderName + "' AND recieverName = '" + recieverName + "'";
			stmt.executeUpdate(query);
			con.close();
			return "success";
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}
	
	public String acceptAnInvitation(String senderName, String recieverName) {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if(con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			if(!checkPlayerAvailability(senderName).equals("success")) {
				return "Player "+ senderName + " is not available!";
			}
			changePlayerStatusToBusy(senderName);
			changePlayerStatusToBusy(recieverName);
			String query = "update invitations set status = 'accepted' where senderName = '"
					+ senderName + "' AND recieverName = '" + recieverName + "'";
			stmt.executeUpdate(query);
			
			String query2 = "update invitations set status = 'canceled' where senderName = '"
					+ senderName + "' AND status = 'waited'";
			stmt.executeUpdate(query2);
			
			String query3 = "update invitations set status = 'canceled' where senderName = '"
					+ recieverName + "' AND status = 'waited'";
			stmt.executeUpdate(query3);
			
			String query4 = "update invitations set status = 'canceled' where recieverName = '"
					+ senderName + "' AND status = 'waited'";
			stmt.executeUpdate(query4);
			
			String query5 = "update invitations set status = 'canceled' where recieverName = '"
					+ recieverName + "' AND status = 'waited'";
			stmt.executeUpdate(query5);
			
			String query6 = "insert into games (playerOneName, playerTwoName)"
					+ " values ('" + senderName + "', '" + recieverName + "')";
			stmt.executeUpdate(query6);
			
			con.close();
			return "success";
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}

	public String checkWaitedRequsts(String playerName) {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if(con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			String query = "select senderName from invitations where recieverName = '"
					+ playerName + "' AND status = 'waited'";
			ResultSet rs = stmt.executeQuery(query);
			rs.last();
			int rows = rs.getRow();
			if (rows == 0) {
				con.close();
				return "0";
			}
			rs.first();
			String senderName = rs.getString("senderName");
			con.close();
			return senderName;
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}
	
	public String checkAcceptedRequsts(String playerName) {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if(con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			String query1 = "select recieverName from invitations where senderName = '"
					+ playerName + "' AND status = 'accepted'";
			ResultSet rs = stmt.executeQuery(query1);
			rs.last();
			int rows = rs.getRow();
			if (rows == 0) {
				con.close();
				return "0";
			}
			rs.first();
			String recieverName = rs.getString("recieverName");
			con.close();
			return recieverName;
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}
	
	public String changePlayerStatusToBusy(String playerName) {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if(con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			String query = "update player set status = 'Busy'"
					+ " where name = '" + playerName + "'";
			stmt.executeUpdate(query);
			con.close();
			return "success";
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}
	
	public String changePlayerStatusToAvailable(String playerName) {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if(con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			String query = "update player set status = 'Available'"
					+ " where name = '" + playerName + "'";
			stmt.executeUpdate(query);
			con.close();
			return "success";
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}
	
	public String checkPlayerAvailability(String playerName) {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if(con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			
			String query = "select name from player where name = '" + playerName + "'"
					+ " And status = 'Available'";
			ResultSet rs = stmt.executeQuery(query);
			rs.last();
			int rows = rs.getRow();
			if (rows == 0) {
				con.close();
				return "Player "+ playerName + " is not available!";
			}
			con.close();
			return "success";
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}
	
	public String checkPlayerColor(String playerName) {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if(con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			String query = "select color from player where name = '" + playerName + "'";
			ResultSet rs = stmt.executeQuery(query);
			rs.last();
			int rows = rs.getRow();
			if (rows == 0) {
				con.close();
				return "Player "+ playerName + " is not available!";
			}
			rs.first();
			String playerColor = rs.getString("color");
			con.close();
			return playerColor;
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}
	
	public String getTurnPlace(String playerOneName, String playerTwoName) {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if(con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			String query = "select turnPlace from games where playerOneName = '"
					+ playerOneName + "' AND playerTwoName = '" + playerTwoName + "'";
			ResultSet rs = stmt.executeQuery(query);
			rs.last();
			int rows = rs.getRow();
			if (rows == 0) {
				con.close();
				return "0";
			}
			rs.first();
			String turnPlace = rs.getString("turnPlace");
			con.close();
			return turnPlace;
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}
	
	public String changeTurnPlace(String playerOneName, String playerTwoName, String newTurnPlace) {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if(con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			String query = "update games set turnPlace = '" + newTurnPlace
					+ "' where playerOneName = '" + playerOneName + "' AND playerTwoName = '" + playerTwoName + "'";
			stmt.executeUpdate(query);
			con.close();
			return "success";
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}
	
	public String playAgain(String playerName) throws IllegalArgumentException {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if(con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();
			
			changePlayerStatusToAvailable(playerName);
			
			String query = "update games set status = 'done'"
					+ " where playerOneName = '" + playerName + "' OR playerTwoName = '" + playerName + "'";
			stmt.executeUpdate(query);
			con.close();
			return "success";
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}		
	}
	
	public String logOutFromServer(String playerName) {
		try {
			Class.forName(dbClass).newInstance();
			Connection con = null;
			con = DriverManager.getConnection(dbUrl, USER, PASS);
			if(con == null) {
				return "Failed to connect to database server!"
						+ "Check out your internet connection.";
			}
			Statement stmt = con.createStatement();

			String query = "delete from player where name = '" + playerName + "'";
			stmt.executeUpdate(query);
			
			String query2 = "delete from invitations where senderName = '" + playerName + "'"
					+ " OR recieverName = '" + playerName + "'";
			stmt.executeUpdate(query2);
			
			String query3 = "delete from games where playerOneName = '" + playerName + "'"
					+ " OR playerTwoName = '" + playerName + "'";
			stmt.executeUpdate(query3);
			
			con.close();
			
			System.exit(0);
			
			return "success";
		}
		catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
	}
	
	public void systemClose() throws IllegalArgumentException {
		System.exit(0);
	}
}